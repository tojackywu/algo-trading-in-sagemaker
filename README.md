# Algorithmic Trading Workshop

This will implement following architecture. 

![chart](assets/chart.png)


## Regions

This workshop has been tested in **us-east-1**.

## Step 0: Set up environment

For the base infrastructure components (SageMaker Notebook, Athena, Glue Tables, S3 Bucket), deploy the following [CloudFormation template](https://gitlab.com/tojackywu/algo-trading-in-sagemaker/-/blob/main/0_Setup/algo-reference.yaml).
First go to [CloudFormation](https://console.aws.amazon.com/cloudformation/home?#/stacks/new?stackName=algotrading) and upload the downloaded CF template. Verify that stackName is **algotrading** before creating the stack and acknowledge that IAM changes will be made.

This step will take ca. 5 minutes.

## Step 1: Load Historical Price Data

The daily datasets can be downloaded and generated in a few minutes:
1. Sample Daily EOD Stock Price Data (from AWS Data Exchange or public data source)


### Step 2: Train a model in Sagemaker

You check the model notebook: **3_Models/Train_Model_Forecast.ipynb**

## Step 3: Backtest, optimze and deploy the model

You can back test the model, optimize the trading strategy parameters, deploy the model in: **2_Strategies/Strategy_ML_Forecast.ipynb**

Following is the deployed model in Sagemaker:
![endpoint](assets/smendpoint1.png)

## Step 4: Create a Lambda to call the model to generate trading signal

You find the Lambda source code: **4_Lambda/GenerateTradingSignal.py**

## Step 5: Create an API Gateway to link the Lambda

After creating API gateway as below, you can get REST api call after receiving new daily OHLC.

![apigateway](assets/api1.png)

Here is a sample to test REST API in https://reqbin.com/:

Request body:
*{   "Body":"0.143955014,0.286410497,0.113964386,0,0.010196813,0.107466417,0.148667827,0.084723524,0.14961991,0.285810684,0.455584902,0.597063418,0.706394636,0.798821797,0.905117151,1,1.35501355,1.763169351,0.332653718,-1.578947368,-0.890396438,1.840758087,-1.837270341,-4.650214155,-6.686626747,-6.686626747,-6.26566416,-6.209248671,-7.425742574,-7.517309594,-8.243375859"
}*

![restapicall](assets/postman1.png)

You can receive the buy or sell recommended possibility.

***happy trading***
