import os
import sys
import traceback
import math
import numpy as np
import pandas as pd

yLen=2
b=0

def data_process(df):
    global yLen
    global b
    dataX=[]
    dataY=[]
    for idx,row in df.iterrows():
        row1=[]
        r=row[1:len(row)-yLen]
        for a in r:
            row1.append(a)
        x=np.array(row1)
        y=np.array(row[len(row)-yLen:])
        b=len(x)
        dataX.append(x)
        dataY.append(y)
    dataX=np.array(dataX).astype(np.float32)
    dataY=np.array(dataY).astype(np.float32)
    return dataX,dataY,b

input_path = 'data_inf.csv'
raw_data = pd.read_csv(input_path)
X, y, b = data_process(raw_data)

print(raw_data)
print("X:")
print (X)
print("y:")
print (y)
print("b:")
print (b)



