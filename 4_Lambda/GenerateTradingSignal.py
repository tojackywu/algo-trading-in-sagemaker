import os
import io
from io import StringIO
import boto3
import json
import csv# grab environment variables

ENDPOINT_NAME = os.environ['ENDPOINT_NAME']
runtime= boto3.client('runtime.sagemaker')
def lambda_handler(event, context):
    print("Received event: " + json.dumps(event, indent=2))
    
    data = json.dumps(event)
    payload = data
    print(event['Body'])

    print('Testing with input: ' + event['Body'])
    body = event['Body'].encode("utf-8")
    print('Testing with body: ')
    print(body)
    # test_string = '0.143955014,0.286410497,0.113964386,0,0.010196813,0.107466417,0.148667827,0.084723524,0.14961991,0.285810684,0.455584902,0.597063418,0.706394636,0.798821797,0.905117151,1,1.35501355,1.763169351,0.332653718,-1.578947368,-0.890396438,1.840758087,-1.837270341,-4.650214155,-6.686626747,-6.686626747,-6.26566416,-6.209248671,-7.425742574,-7.517309594,-8.243375859'
    response = runtime.invoke_endpoint(EndpointName=ENDPOINT_NAME,
                                       ContentType='text/csv',
                                       Body=body)
                                       
                                      

    result = response['Body'].read().decode()
    print('infererence result is:')
    print(result)
    
    return { 
        'statusCode': 200,
        'body': result
    }